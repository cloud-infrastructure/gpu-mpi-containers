# mpi_learn deployment infrastructure

Deploy [mpi_learn](https://github.com/svalleco/mpi_learn) on a GPU Kubernetes cluster

## Instructions

1. Create a Magnum Kubernetes cluster with a GPU flavor
2. Deploy the [Nvidia GPU device plugin](https://gitlab.cern.ch/cloud-infrastructure/nvidia-system-container/blob/coreos/daemonsets/nvidia-gpu-device-plugin.yaml)
3. Configure the cluster-specific [mpirun parameters in mpi/default-hostfile](build-image/mpi/default-hostfile) (more below)
4. Rebuild and push the docker image ([docker_build](build-image/docker_build))
5. Deploy the [resources in "deployment"](kubernetes/deployment)
  - Tune the number of MPI processes to launch in the [master's definition](kubernetes/deployment/svalleco-original-master.yaml),
    by correspondingly changing the argument to the starting script
  - The number of MPI processes must fit in the available slots defined in step 3

### Configure cluster-specific mpirun parameters

Currently, the mpirun configuration is static and the mpi processes see their host's network stack. For every node in the cluster where an MPI process can be sent there should be a line in [mpi/default-hostfile](build-image/mpi/default-hostfile) with its IP and number of GPUs (1-1 GPU/slot correspondence).

## Building the docker image

The docker images carry ssh keys baked in to facilitate the MPI communication. For security purposes, these keys are generated on the fly before `docker build` and are then deleted. To rebuild the docker images, run [docker_build](build-image/docker_build). You must first do `docker login` to an image registry and you can change the default registry/image name (but then the kubernetes resources have to be changed accordingly).
