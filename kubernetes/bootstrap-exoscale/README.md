# Bootstrap a Kubernetes cluster on exoscale with kubeadm

[source of instructions](https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/)

For every VM:
1. Disable firewall! (could be optimized to only open some ports, but complicates networking)
1. [Install docker](kubernetes/bootstrap-exoscale/install-docker-centos.sh)
1. [Install kubelet/kubeadm/kubectl](kubernetes/bootstrap-exoscale/install-kubelet-centos.sh)

Init master:
1. `kubeadm config images pull`
1. `kubeadm init --pod-network-cidr=10.244.0.0/16` (chosen flannel networking)
    * might complain for docker version, try with `--ignore-preflight-errors=SystemVerification`
1. `export KUBECONFIG=/etc/kubernetes/admin.conf`
1. Note the generated `kubeadm join` cmd, it contains a secret token
1. Pod networking: flannel
1. For running pods also on master, untaint: `kubectl taint nodes --all node-role.kubernetes.io/master-`

Join worker VMs:
1. For every worker VM, run the `kubeadm join ...` generated before by `kubeadm init`
    * To see the tokens again, run on master: `kubeadm token list`
1. Check notes for scaling the cluster

Finally, install the [nvidia device plugin daemonset for Centos 7](https://gitlab.cern.ch/cloud-infrastructure/nvidia-driver-container/tree/coreos/daemonsets/nvidia-gpu-device-plugin-cc7.yaml).


Get cluster credentials to run `kubectl` from your laptop. On the laptop:
```
scp root@<master>:/etc/kubernetes/admin.conf .
kubectl --kubeconfig ./admin.conf get nodes
```

## Pod networking: flannel

```
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/bc79dd1505b0c8681ece4de4c0d86c5cd2643275/Documentation/kube-flannel.yml
```
Check after install that CoreDNS is running, then continue.

## Scaling the cluster

Tokens expire after 24h. To join a new VM later than that, generate new tokens:
`kubeadm token create`

If you don’t have the value of `--discovery-token-ca-cert-hash`, you can get it by
running the following command chain on the master node:

```
openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | \
  openssl rsa -pubin -outform der 2>/dev/null | \
  openssl dgst -sha256 -hex | sed 's/^.* //'
  ```
