#!/bin/bash

# Argument: The number of MPI processes to start. Must fit in the cluster's available
#   GPUs, which in turn should inform the parameters in mpi/default-hostfile
mpi_numprocs=${1:-2}
./make_model.sh
#/usr/sbin/sshd -p ${SSH_PORT}
./run_model.sh "$mpi_numprocs"
