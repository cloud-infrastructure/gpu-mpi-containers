#!/bin/bash
cd /ml/mpi_learn
export TERM=linux
mpirun -tag-output -n "$1" \
  -bind-to none -map-by slot \
  -x LD_LIBRARY_PATH -x PATH -x TERM -x NCCL_DEBUG=INFO -x NCCL_SOCKET_IFNAME=eth0 \
  -mca pml ob1 -mca btl ^openib \
  python3 MPIDriver.py mnist_arch.json train_mnist.list test_mnist.list --loss categorical_crossentropy --epochs 5 --tf
