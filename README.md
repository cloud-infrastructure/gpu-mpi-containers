# mpi_learn deployment infrastructure

Deploy [mpi_learn](https://github.com/svalleco/mpi_learn) on a GPU cluster

### Different deployment options

mpi_learn can be deployed in a few different ways. A kubernetes cluster is preferable, but if it is impractical there is also a docker-only option.

- For a bare Kubernetes deployment, see [kubernetes](kubernetes)
- For a docker-only deployment, see [docker-only](docker-only)
